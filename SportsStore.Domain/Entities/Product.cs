﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public class Product
    {
        [HiddenInput(DisplayValue =false)]
        public int ProductID { get; set; }

        [Required(ErrorMessage ="Podaj nazwe.")]
        [Display(Name="Nazwa")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Podaj opis.")]
        [DataType(DataType.MultilineText),Display(Name="Opis")]
        public string Description { get; set; }

        [Required]
        [Range(0.01, double.MaxValue,ErrorMessage ="Podaj dodatnia cene.")]
        [Display(Name="Cena")]
        public decimal Price { get; set; }

        [Required(ErrorMessage ="Podaj kategorie.")]
        [Display(Name="Kategoria")]
        public string Category { get; set; }
    }
}

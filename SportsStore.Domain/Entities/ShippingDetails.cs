﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SportsStore.Domain.Entities
{

/*
 *name
 * line1
 * lin2
 * lin3
 * city 
 * state
 * zip
 * country
 * gift wrap
 * 
*/
    public class ShippingDetails
    {
        [Required(ErrorMessage ="Podaj Imię i nazwisko.")]
        [Display(Name="Imię i nazwisko")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Proszę podać adresu.")]
        [Display(Name="Adres")]
        public string AdressLine { get; set; }
        [Required(ErrorMessage ="Podaj miasto.")]
        [Display(Name="Miasto")]
        public string City { get; set; }
        [Required(ErrorMessage ="Podaj kod pocztowy.")]
        [RegularExpression(@"[0-9]{2}-[0-9]{3}",ErrorMessage ="Podaj prawidłowy kod pocztowy.")]
        [Display(Name="Kod pocztowy")]
        public string Zip { get; set; }
        [Required(ErrorMessage ="Podaj kraj.")]
        [Display(Name="Kraj")]
        public string Country { get; set; }
        public bool GiftWrap { get; set; }

    }
}

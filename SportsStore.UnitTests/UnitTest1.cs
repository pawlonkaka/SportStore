﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using SportsStore.WebUI.Models;
using SportsStore.WebUI.HtmlHelpers;
using SportsStore.Domain.Abstract;
using Moq;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;
using System.Linq;
using System.Collections.Generic;
using static SportsStore.Domain.Entities.Cart;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void Can_Send_Pagination_View_Model()
        {
            // przygotowanie
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                     new Product {ProductID = 1, Name = "P1"},
                     new Product {ProductID = 2, Name = "P2"},
                     new Product {ProductID = 3, Name = "P3"},
                     new Product {ProductID = 4, Name = "P4"},
                     new Product {ProductID = 5, Name = "P5"}
            });


            // przygotowanie
            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;
            // działanie
            ProductsListViewModel result = (ProductsListViewModel)controller.List(null, 2).Model;
            // asercje
            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);
        }

        [TestMethod]
        public void Can_Filter_Products()
        {
            // przygotowanie
            // — utworzenie imitacji repozytorium
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                 new Product {ProductID = 1, Name = "P1", Category = "Cat1"},
                 new Product {ProductID = 2, Name = "P2", Category = "Cat2"},
                 new Product {ProductID = 3, Name = "P3", Category = "Cat1"},
                 new Product {ProductID = 4, Name = "P4", Category = "Cat2"},
                 new Product {ProductID = 5, Name = "P5", Category = "Cat3"}
            });
            // przygotowanie — utworzenie kontrolera i ustawienie 3-elementowej strony
            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;
            // działanie
            Product[] result = ((ProductsListViewModel)controller.List("Cat2", 1).Model).Products.ToArray();
            // asercje
            Assert.AreEqual(result.Length, 2);
            Assert.IsTrue(result[0].Name == "P2" && result[0].Category == "Cat2");
            Assert.IsTrue(result[1].Name == "P4" && result[1].Category == "Cat2");
        }

        [TestMethod]
        public void Can_Create_Categories()
        {
            // przygotowanie
            // — tworzenie imitacji repozytorium
            Mock<IProductRepository> mock = new Mock<IProductRepository>();

            mock.Setup(m => m.Products).Returns(new Product[] {
                 new Product {ProductID = 1, Name = "P1", Category = "Jabłka"},
                 new Product {ProductID = 2, Name = "P2", Category = "Jabłka"},
                 new Product {ProductID = 3, Name = "P3", Category = "Śliwki"},
                 new Product {ProductID = 4, Name = "P4", Category = "Pomarańcze"},
             });
            // przygotowanie — utworzenie kontrolera
            NavController target = new NavController(mock.Object);
            // działanie — pobranie zbioru kategorii
            string[] results = ((IEnumerable<string>)target.Menu().Model).ToArray();
            // asercje
            Assert.AreEqual(results.Length, 3);
            Assert.AreEqual(results[0], "Jabłka");
            Assert.AreEqual(results[1], "Pomarańcze");
            Assert.AreEqual(results[2], "Śliwki");
        }

        [TestClass]
        public class CartTests
        {
            [TestMethod]
            public void Can_Add_New_Lines()
            {
                // przygotowanie — utworzenie produktów testowych
                Product p1 = new Product { ProductID = 1, Name = "P1" };
                Product p2 = new Product { ProductID = 2, Name = "P2" };
                // przygotowanie — utworzenie nowego koszyka
                Cart target = new Cart();
                // działanie
                target.AddItem(p1, 1);
                target.AddItem(p2, 1);
                CartLine[] results = target.Lines.ToArray();
                // asercje
                Assert.AreEqual(results.Length, 2);
                Assert.AreEqual(results[0].Product, p1);
                Assert.AreEqual(results[1].Product, p2);
            }
        }
        [TestMethod]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            // przygotowanie — tworzenie produktów testowych
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };
            // przygotowanie — utworzenie nowego koszyka
            Cart target = new Cart();
            // działanie
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            target.AddItem(p1, 10);
            CartLine[] results = target.Lines.OrderBy(c => c.Product.ProductID).ToArray();
            // asercje
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Quantity, 11);
            Assert.AreEqual(results[1].Quantity, 1);
        }

        [TestMethod]
        public void Can_Add_To_Cart()
        {
            // przygotowanie — tworzenie imitacji repozytorium
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                 new Product {ProductID = 1, Name = "P1", Category = "Jab"},
            }.AsQueryable());

            // przygotowanie — utworzenie koszyka
            Cart cart = new Cart();
            // przygotowanie — utworzenie kontrolera
            CartController target = new CartController(mock.Object,null);
            // działanie — dodanie produktu do koszyka
            target.AddToCart(cart, 1, null);
        }

        [TestMethod]
        public void Can_View_Cart_Contents()
        {
            // przygotowanie — utworzenie koszyka
            Cart cart = new Cart();
            // przygotowanie — utworzenie kontrolera
            CartController target = new CartController(null,null);
            // działanie — wywołanie metody akcji Index
            CartIndexViewModel result
            = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;
            // asercje
            Assert.AreSame(result.Cart, cart);
            Assert.AreEqual(result.ReturnUrl, "myUrl");
        }

        [TestMethod]
        public void Cannot_Checkout_empty_card()
        {
            //Przygotowanie
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();

            ShippingDetails shipping = new ShippingDetails();
            CartController cartController = new CartController(null, mock.Object);

            //ACT
            ViewResult result = cartController.Checkout(cart, shipping);

            //ASSERT
            mock.Verify(m=>m.ProcessOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()),Times.Never());

            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Can_Save_Changes()
        {
            //przygotowanie
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController target = new AdminController(mock.Object);

            Product product = new Product { Name = "Test" };

            //dzialanie (act)
            ActionResult result = target.Edit(product);

            //asercja (assert)

            mock.Verify(m => m.SaveProduct(product));

            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }
    }
}
